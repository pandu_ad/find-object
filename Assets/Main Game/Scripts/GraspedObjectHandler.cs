﻿using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraspedObjectHandler : MonoBehaviour {
    public bool isStatonary;
    private Vector3 position;
    private Quaternion rotation;
    GraspedObject graspedObject;
    InteractionBehaviour interactionBehaviour;

    // Use this for initialization
    void Start () {
        position = transform.position;
        rotation = transform.rotation;

        interactionBehaviour = GetComponent<InteractionBehaviour>();

        GameObject GO = GameObject.Find("GraspedObject");
        graspedObject = GO.GetComponent("GraspedObject") as GraspedObject;
    }
	
	// Update is called once per frame
	void Update () {
        if (interactionBehaviour.IsBeingGrasped)
        {
            graspedObject.ItemID = gameObject.name;
        }
        else if(isStatonary)
        {
            resetObject();
        }
	}

    void resetObject()
    {
        gameObject.transform.position = position;
        transform.transform.rotation = rotation;
    }
}
