﻿using UnityEngine;
using NUnit.Framework;
using System.Collections;

namespace Leap.Unity.Interaction.Tests
{

    public class BaseCallGuardTest
    {
        private const string KEY_A = "KeyA";
        private const string KEY_B = "KeyB";

        private BaseCallGuard _guard;

        [SetUp]
        public void Setup()
        {
            _guard = new BaseCallGuard();
        }

        [TearDown]
        public void Teardown()
        {
            _guard = null;
        }

        [Test]
        public void BaseCalled()
        {
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_A);
            _guard.AssertBaseCalled();
        }

        [Test]
        public void Recursive1()
        {
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_A);
            _guard.Begin(KEY_B);
            _guard.NotifyBaseCalled(KEY_B);
            _guard.AssertBaseCalled();
            _guard.AssertBaseCalled();
        }

        [Test]
        public void Recursive2()
        {
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_A);
            _guard.Begin(KEY_B);
            _guard.NotifyBaseCalled(KEY_B);
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_A);
            _guard.Begin(KEY_B);
            _guard.NotifyBaseCalled(KEY_B);
            _guard.AssertBaseCalled();
            _guard.AssertBaseCalled();
            _guard.AssertBaseCalled();
            _guard.AssertBaseCalled();
        }

        [Test]
        public void Recursive_BaseNotCalled1()
        {
            _guard.Begin(KEY_A);
            _guard.Begin(KEY_B);
            _guard.NotifyBaseCalled(KEY_B);
            _guard.AssertBaseCalled();

            Assert.That(() => _guard.AssertBaseCalled(),
                Throws.TypeOf<BaseNotCalledException>());
        }

        [Test]
        public void Recursive_BaseNotCalled2()
        {
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_A);
            _guard.Begin(KEY_B);
            _guard.AssertBaseCalled();

            Assert.That(() => _guard.AssertBaseCalled(),
                Throws.TypeOf<BaseNotCalledException>());
        }

        [Test]
        public void BaseNotCalled()
        {
            _guard.Begin(KEY_A);

            Assert.That(() => _guard.AssertBaseCalled(),
                Throws.TypeOf<BaseNotCalledException>());
        }

        [Test]
        public void WrongBaseCalled()
        {
            _guard.Begin(KEY_A);
            _guard.NotifyBaseCalled(KEY_B);

            Assert.That(() => _guard.AssertBaseCalled(),
                Throws.TypeOf<WrongBaseCalledException>());
        }

        [Test]
        public void BeginNotCalled_Nofity()
        {
            Assert.That(() => _guard.NotifyBaseCalled(KEY_B),
                Throws.TypeOf<BeginNotCalledException>());
        }

        [Test]
        public void BeginNotCalled_Assert()
        {
            Assert.That(() => _guard.AssertBaseCalled(),
                Throws.TypeOf<BeginNotCalledException>());
        }

    }
}
