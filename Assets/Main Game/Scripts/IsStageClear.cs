﻿using Hover.Core.Items;
using Hover.InterfaceModules.Cast;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsStageClear : MonoBehaviour {
    GameObject stageClear;

    // Use this for initialization
    void Start () {
        stageClear = GameObject.Find("StageClear");
        stageClear.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        bool isClear = true;

        HovercastInterface hovercastInterface = GameObject.Find("Hovercast").GetComponent<HovercastInterface>();
        foreach(Transform child in hovercastInterface.ActiveRow.gameObject.transform)
        {
            if (child.parent == hovercastInterface.ActiveRow.gameObject.transform)
            {
                if(child.gameObject.GetComponent<HoverItemData>() != null && child.gameObject.GetComponent<HoverItemData>().Id != "Move")
                    isClear = isClear && !child.gameObject.GetComponent<HoverItemData>().IsEnabled;
            }
        }

        if (isClear)
        {
            stageClear.SetActive(true);
        }
    }
}
