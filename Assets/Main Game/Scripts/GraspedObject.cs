﻿using Hover.Core.Items;
using Leap.Unity.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GraspedObject : MonoBehaviour
{
    public string ItemID;
    bool isDone;
    GraspedObject graspedObject;
    InteractionManager interactionManager;

    // Use this for initialization
    void Start()
    {
        isDone = false;

        GameObject GO = GameObject.Find("GraspedObject");
        graspedObject = GO.GetComponent<GraspedObject>();
        interactionManager = GameObject.Find("InteractionManager").GetComponent<InteractionManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!interactionManager.IsAnyObjectGrasped)
        {
            graspedObject.ItemID = "";
        }
    }

    public void CheckGraspedItem(string ID)
    {
        isDone = ItemID.Equals(ID);

        Player player = GameObject.Find("EventSystem").GetComponent<Player>();
        if (isDone)
        {
            player.PlayClip(0);
        }
        else
        {
            player.PlayClip(1);
        }
    }

    public void IsDone(GameObject doneSign)
    {
        doneSign.SetActive(isDone);
    }

    public void DisableHoverItem(GameObject hoverItem)
    {
        hoverItem.GetComponent<HoverItemData>().IsEnabled = !isDone;

        isDone = false;
    }
}
