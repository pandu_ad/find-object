﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public AudioClip[] audioClip;
	
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayClip(int clip) {
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayOneShot(audioClip[clip]);
    }
}
